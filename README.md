# sod cmus

A simple web interface for a local cmus using cmus-remote.

![example](example.png)

## Build & Run

Prerequisites: [Stack](https://haskellstack.org).

```sh
stack build && stack exec -- sod-cmus --port 1234
```

Will run the website on port 1234.

