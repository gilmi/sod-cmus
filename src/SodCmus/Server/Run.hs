{-# LANGUAGE TemplateHaskell, LambdaCase, OverloadedStrings, TupleSections #-}

module SodCmus.Server.Run where

import Web.Spock
import Web.Spock.Config
import Web.Spock.Lucid
import qualified Lucid as L

import Data.Tuple
import Data.Foldable
import Control.Arrow ((&&&), first)
import Control.Applicative
import Control.Monad
import Control.Monad.Trans
import Data.Maybe
import Data.Monoid
import Control.Concurrent (threadDelay)
import Control.Concurrent.Async
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as T
import qualified Data.Map as M
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import qualified Network.Mime as Mime (defaultMimeLookup)
import Data.FileEmbed
import System.Process.Typed

import SodCmus.Server.Html


assetsDir :: [(FilePath, BS.ByteString)]
assetsDir = $(embedDir "assets")

data Session = EmptySession

run :: Int -> IO ()
run port = do
  spockCfg <- defaultSpockCfg EmptySession PCNoDatabase ()
  runSpock port (spock spockCfg app)

app :: SpockM () Session () ()
app = do
  for_ assetsDir $ \(path_, file_) -> do
    get (static $ "assets/" <> path_) $
      serveBytes path_ file_

  get root $ do
    (code, out, err) <- readProcess $ shell "cmus-remote --query"
    lucid $ front
      ( code
      , T.decodeUtf8 $ BSL.toStrict out
      , T.decodeUtf8 $ BSL.toStrict err
      )

  get "play" $ do
    runProcess_ $ shell "cmus-remote --play"
    redirect "/"
  get "pause" $ do
    runProcess_ $ shell "cmus-remote --pause"
    redirect "/"
  get "prev" $ do
    runProcess_ $ shell "cmus-remote --prev"
    redirect "/"
  get "next" $ do
    runProcess_ $ shell "cmus-remote --next"
    redirect "/"
  get "vol-down" $ do
    runProcess_ $ shell "cmus-remote --volume -5%"
    redirect "/"
  get "vol-up" $ do
    runProcess_ $ shell "cmus-remote --volume +5%"
    redirect "/"

serveBytes :: MonadIO m => FilePath -> BS.ByteString -> ActionT m a
serveBytes path file' = do
  let mime = Mime.defaultMimeLookup (T.pack path)
  setHeader "content-type" $ T.decodeUtf8 mime
  bytes file'
