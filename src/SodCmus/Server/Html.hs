{-# LANGUAGE LambdaCase, OverloadedStrings, RecordWildCards #-}

module SodCmus.Server.Html where

import Control.Arrow ((>>>))
import qualified Lucid as H
import Lucid.Html5
import Control.Monad
import Data.Text (Text, pack, unpack)
import qualified Data.Text as T
import Data.Monoid
import Data.List (find)
import System.Exit

type Html = H.Html ()


-- | A page template
template :: Text -> Html -> Html
template title body =
  doctypehtml_ $ do
    head_ $ do
      meta_ [ charset_ "utf-8" ]

      title_ (H.toHtml $ title)

      meta_ [ name_ "viewport", content_ "width=device-width, initial-scale=1" ]

      link_ [ rel_ "stylesheet", type_ "text/css", href_ "/assets/css/style.css" ]

    body_ $ do
      div_ [class_ "container"] $ do
        H.div_ [ H.class_ "top row" ] $ do
          header_ $ do
            h1_ (H.a_ [ H.href_ "/" ] "sod cmus server")

        div_ [id_ "main"] body

front :: (ExitCode, T.Text, T.Text) -> Html
front (code, out, err) = do
  case code of
    ExitFailure c -> do
      h1_ ("Failure " <> H.toHtml (show c))
      p_ $ H.toHtml err
    _ -> do
      let Status{..} = parse out
      template (status <> ": " <> title <> " / " <> artist) $ do
        h2_ $ H.toHtml $ "Status: " <> status
        div_ $ do
          if T.null title || T.null artist
            then
              p_ [ class_ "file" ] $ H.toHtml file
            else do
              p_ [ class_ "title" ] $ H.toHtml title
              p_ [ class_ "seperator" ] $ "/"
              p_ [ class_ "artist" ] $ H.toHtml artist
        div_ $ do
          unless (T.null album) $ do
            p_ [ class_ "album" ] $ H.toHtml $ "Album: " <> album
        div_ $ do
          ul_ $ do
            li_ $ a_ [ href_ "prev" ] "⏮ Prev"
            li_ $ a_ [ href_ "pause" ] "⏸ Pause"
            li_ $ a_ [ href_ "play" ] "▶ Play"
            li_ $ a_ [ href_ "next" ] "⏭ Next"
          ul_ $ do
            li_ $ a_ [ href_ "vol-down" ] "🔉 Volume Down"
            li_ $ a_ [ href_ "vol-up" ] "🔊 Volume Up"

data Status
  = Status
  { artist :: T.Text
  , title :: T.Text
  , album :: T.Text
  , status :: T.Text
  , file :: T.Text
  }
  deriving Show

parse :: T.Text -> Status
parse content =
  let
    ls = map T.words $ T.lines content
    status = T.unwords $ flip concatMap ls $ \case
      "status":rest -> rest
      _ -> []
    artist = T.unwords $ flip concatMap ls $ \case
      "tag":"artist":rest -> rest
      _ -> []
    title = T.unwords $ flip concatMap ls $ \case
      "tag":"title":rest -> rest
      _ -> []
    album = T.unwords $ flip concatMap ls $ \case
      "tag":"album":rest -> rest
      _ -> []
    file = T.unwords $ flip concatMap ls $ \case
      "file":rest -> rest
      _ -> []
  in
    Status
      { artist = artist
      , title  = title
      , album  = album
      , status = status
      , file   = file
      }
